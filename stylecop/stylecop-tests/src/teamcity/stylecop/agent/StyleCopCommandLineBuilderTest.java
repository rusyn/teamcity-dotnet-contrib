/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import jetbrains.buildServer.agent.BuildAgentConfiguration;
import jetbrains.buildServer.util.StringUtil;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import teamcity.stylecop.common.StyleCopConstants;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(JUnit4.class)
public class StyleCopCommandLineBuilderTest {
    private Map<String, String> myRunParameters;
    private File myXmlReportFile;
    private File myCheckoutDirectory;
    private String myExpectedPostfix;
    private String myExpectedPrefix;
    private BuildAgentConfiguration mockBuildAgentConfiguration;
    private Mockery context;

    @Before
    public void prepareForTest()
            throws Exception {
        context = new Mockery();
        myRunParameters = new HashMap<String, String>();

        mockBuildAgentConfiguration = context.mock(BuildAgentConfiguration.class);
        context.checking(new Expectations() {
            {
                oneOf(mockBuildAgentConfiguration)
                        .getAgentPluginsDirectory();
            }
        });

        myXmlReportFile = new File("C:\\a\\b");
        myCheckoutDirectory = new File("C:\\checkout");
        myExpectedPrefix = "[/p:";
        myExpectedPostfix = "]";
    }

    @Test
    public void styleCopRoot()
            throws Exception {
        System.out.println("styleCopRoot");
        myRunParameters.put(StyleCopConstants.SETTINGS_STYLECOP_PATH, "a");
        assertCmdArgs("StyleCopPath=a");
    }

    @Test
    public void styleCopRootWithQuotes()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_STYLECOP_PATH, "\"a\"");
        assertCmdArgs("StyleCopPath=a");
    }

    @Test
    public void file()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILES, "file1.csproj");
        assertCmdArgs("IncludeFiles=" + getAbsolutePath("file1.csproj"));
    }

    @Test
    public void absoluteFile()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILES, "C:\\checkout\\file1.csproj");
        assertCmdArgs("IncludeFiles=" + getAbsolutePath("file1.csproj"));
    }

    @Test
    public void files()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILES, "file1.csproj file2.csproj");
        assertCmdArgs("IncludeFiles=" + getAbsolutePath("file1.csproj") + "%3B" + getAbsolutePath("file2.csproj"));
    }

    @Test
    public void filesWithQuotes()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILES, "\"my dir \\file1.csproj\" file2.csproj");
        assertCmdArgs(
                "IncludeFiles=" + getAbsolutePath("my dir \\file1.csproj") + "%3B" + getAbsolutePath("file2.csproj"));
    }

    @Test
    public void excludeFile()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILESEXCLUDE, "file1.csproj");
        assertCmdArgs("ExcludeFiles=" + getAbsolutePath("file1.csproj"));
    }

    @Test
    public void excludeAbsoluteFile()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILESEXCLUDE, "C:\\checkout\\file1.csproj");
        assertCmdArgs("ExcludeFiles=" + getAbsolutePath("file1.csproj"));
    }

    @Test
    public void excludeFiles()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILESEXCLUDE, "file1.csproj file2.csproj");
        assertCmdArgs("ExcludeFiles=" + getAbsolutePath("file1.csproj") + "%3B" + getAbsolutePath("file2.csproj"));
    }

    @Test
    public void excludeFilesWithQuotes()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FILESEXCLUDE, "\"my dir \\file1.csproj\" file2.csproj");
        assertCmdArgs(
                "ExcludeFiles=" + getAbsolutePath("my dir \\file1.csproj") + "%3B" + getAbsolutePath("file2.csproj"));
    }

    @Test
    public void maxViolationsCount()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_MAXVIOLATIONCOUNT, "3");
        assertCmdArgs("MaxViolationCount=3");
    }

    @Test
    public void overrideSettingsFile()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_OVERRIDESETTINGSFILE, "C:\\settings.stylecop");
        assertCmdArgs("OverrideSettingsFile=C:\\settings.stylecop");
    }

    @Test
    public void OverrideSettingsFileQuote()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_OVERRIDESETTINGSFILE, "\"C:\\settings.stylecop\"");
        assertCmdArgs("OverrideSettingsFile=C:\\settings.stylecop");
    }

    @Test
    public void DefineConstants()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_DEFINECONSTANTS, "DEBUG;SILVERLIGHT");
        assertCmdArgs("DefineConstants=DEBUG|SILVERLIGHT");
    }

    @Test
    public void DefineConstantsQuotes()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_DEFINECONSTANTS, "\"DEBUG\";\"SILVERLIGHT\"");
        assertCmdArgs("DefineConstants=DEBUG|SILVERLIGHT");
    }

    @Test
    public void AddinPath()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_ADDITIONALADDINPATHS, "b");
        assertCmdArgs("AdditionalAddinPaths=C:\\checkout\\b");
    }

    @Test
    public void AddinPaths()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_ADDITIONALADDINPATHS, "a b\\c");
        assertCmdArgs("AdditionalAddinPaths=C:\\checkout\\a|C:\\checkout\\b\\c");
    }

    @Test
    public void AddinPathsQuotes()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_ADDITIONALADDINPATHS, "\"a\" \"b\\c\"");
        assertCmdArgs("AdditionalAddinPaths=C:\\checkout\\a|C:\\checkout\\b\\c");
    }

    @Test
    public void CacheResults()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_CACHERESULTS, "True");
        assertCmdArgs("CacheResults=True");
    }

    @Test
    public void ForceFullAnalysis()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_FORCEFULLANALYSIS, "True");
        assertCmdArgs("ForceFullAnalysis=True");
    }

    @Test
    public void Debug()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_DEBUG, "True");
        assertCmdArgs("Debug=True");
    }

    @Test
    public void TreatErrorsAsWarnings()
            throws Exception {
        myRunParameters.put(StyleCopConstants.SETTINGS_TREATERRORSASWARNINGS, "True");
        assertCmdArgs("TreatErrorsAsWarnings=True");
    }

    private void assertCmdArgs(final String expected)
            throws Exception {
        final String filesSetting = myRunParameters.get(StyleCopConstants.SETTINGS_FILES);
        final String excludeFilesSetting = myRunParameters.get(StyleCopConstants.SETTINGS_FILESEXCLUDE);
        final List<String> files = filesSetting != null
                ? StringUtil.splitCommandArgumentsAndUnquote(filesSetting)
                : new ArrayList<String>();
        final List<String> excludeFiles = excludeFilesSetting != null
                ? StringUtil.splitCommandArgumentsAndUnquote(excludeFilesSetting)
                : new ArrayList<String>();

        final StyleCopCommandLineBuilder commandLineBuilder =
                new StyleCopCommandLineBuilder(myXmlReportFile,
                                               files,
                                               excludeFiles,
                                               mockBuildAgentConfiguration,
                                               myRunParameters,
                                               myCheckoutDirectory);

        final List<String> args = commandLineBuilder.getArguments();

        StringBuilder result = new StringBuilder();
        for (String chunk : args) {
            // Could be done in more efficient way
            if (chunk.startsWith("/p:")) {
                result.append("[").append(chunk).append("]");
                break;
            }
        }

        final String expectedResult = myExpectedPrefix + expected + myExpectedPostfix;
        Assert.assertEquals(expectedResult, result.toString());
    }

    private String getAbsolutePath(String fileName) {
        return new File(myCheckoutDirectory, fileName).getAbsolutePath();
    }
}
