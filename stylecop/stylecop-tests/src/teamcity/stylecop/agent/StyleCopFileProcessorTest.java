/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class StyleCopFileProcessorTest {
    @Test
    public void none()
            throws Exception {
        assertFile("0Violations.xml", 0);
    }

    @Test
    public void many()
            throws Exception {
        assertFile("16Violations.xml", 16);
    }

    private void assertFile(final String fileName, final int expectedCount)
            throws Exception {
        final String testFile = getTestDataPath(fileName);

        final StyleCopFileProcessor processor = new StyleCopFileProcessor(new File(testFile));
        processor.processReport();

        Assert.assertEquals(expectedCount, processor.getViolationsCount());
    }

    private String getTestDataPath(final String fileName) {
        File file = new File("testData/" + fileName);

        return file.getAbsolutePath();
    }
}
