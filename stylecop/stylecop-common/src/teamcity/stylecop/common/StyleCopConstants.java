/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.common;

public interface StyleCopConstants {

  String RUNNER_TYPE = "StyleCop";
  String RUNNER_DISPLAY_NAME = "StyleCop";
  String RUNNER_DESCRIPTION = "StyleCop Code Styling Analysis Tool (.NET)";

  String REPORT_FILE = "stylecop-report.html";
  String OUTPUT_FILE = "stylecop-result.xml";

  String SETTINGS_FILES = "stylecop.files";
  String SETTINGS_FILESEXCLUDE = "stylecop.filesexclude";
  String SETTINGS_STYLECOP_PATH = "stylecop.path";
  String SETTINGS_FORCEFULLANALYSIS = "stylecop.forcefullanalysis";
  String SETTINGS_DEFINECONSTANTS = "stylecop.defineconstants";
  String SETTINGS_TREATERRORSASWARNINGS = "stylecop.treaterrorsaswarnings";
  String SETTINGS_CACHERESULTS = "stylecop.cacheresults";
  String SETTINGS_OVERRIDESETTINGSFILE = "stylecop.overridesettingsfile";
  String SETTINGS_DEBUG = "stylecop.debug";
  String SETTINGS_XSLTFILE = "stylecop.xsltfile";
  String SETTINGS_MAXVIOLATIONCOUNT = "stylecop.maxviolationcount";
  String SETTINGS_ADDITIONALADDINPATHS = "stylecop.additionaladdinpaths";

  String DEFAULT_FORCEFULLANALYSIS = "True";
  String DEFAULT_CACHERESULTS = "True";
  String DEFAULT_MAXVIOLATIONCOUNT = "0";
}
