/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.SystemInfo;
import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.BuildFinishedStatus;
import jetbrains.buildServer.agent.artifacts.ArtifactsWatcher;
import jetbrains.buildServer.agent.inspections.InspectionReporter;
import jetbrains.buildServer.agent.runner.BuildServiceAdapter;
import jetbrains.buildServer.agent.runner.ProgramCommandLine;
import jetbrains.buildServer.util.AntPatternFileFinder;
import jetbrains.buildServer.messages.DefaultMessagesInfo;
import jetbrains.buildServer.messages.serviceMessages.ServiceMessage;
import jetbrains.buildServer.util.FileUtil;
import jetbrains.buildServer.util.PropertiesUtil;
import jetbrains.buildServer.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import teamcity.stylecop.common.StyleCopConstants;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StyleCopBuildService extends BuildServiceAdapter {
  private final ArtifactsWatcher myArtifactsWatcher;
  private final InspectionReporter myInspectionReporter;
  private File myOutputDirectory;
  private File myXmlReportFile;
  private File myHtmlReportFile;

  public StyleCopBuildService(final ArtifactsWatcher artifactsWatcher, final InspectionReporter inspectionReporter) {
    myArtifactsWatcher = artifactsWatcher;
    myInspectionReporter = inspectionReporter;

  }

  @Override
  public void afterInitialized() throws RunBuildException {
    super.afterInitialized();

    try {
      myOutputDirectory = FileUtil.createTempFile(getBuildTempDirectory(), "Stylecop-output-", "", false);
      if (!myOutputDirectory.mkdirs()) {
        throw new RuntimeException("Unable to create temp output directory " + myOutputDirectory);
      }

      myXmlReportFile = new File(myOutputDirectory, StyleCopConstants.OUTPUT_FILE);
      myHtmlReportFile = new File(myOutputDirectory, StyleCopConstants.REPORT_FILE);
    } catch (IOException e) {
      final String message = "Unable to create temporary file in " +
          getBuildTempDirectory() + " for stylecop: " +
          e.getMessage();

      Logger.getInstance(getClass().getName()).error(message, e);
      throw new RunBuildException(message);
    }
  }

  @Override
  public void beforeProcessStarted() throws RunBuildException {
    getLogger().progressMessage("Running StyleCop");
  }

  private Boolean importInspectionResults() throws Exception {
    Boolean success = true;
    final Map<String, String> runParameters = getRunnerParameters();

    getLogger().progressMessage("Importing inspection results");

    final StyleCopFileProcessor fileProcessor =  new StyleCopFileProcessor(myXmlReportFile);

    fileProcessor.processReport();

    final int violations = fileProcessor.getViolationsCount();

    boolean limitReached = false;

    final boolean treatErrorsAsWarnings = PropertiesUtil.getBoolean(runParameters.get(StyleCopConstants.SETTINGS_TREATERRORSASWARNINGS));
    final Integer errorLimit = PropertiesUtil.parseInt(runParameters.get(StyleCopConstants.SETTINGS_MAXVIOLATIONCOUNT));
    if (errorLimit != null && errorLimit >= 0 && violations > errorLimit) {
      getLogger().error("Max violation limit reached: found " + violations + " violations, limit " + errorLimit);
      limitReached = true;
    }

    if (limitReached) {
      getLogger().message(ServiceMessage.asString("buildStatus", new HashMap<String, String>() {{
        put("text", "StyleCop violations: " + violations);
        put("status", treatErrorsAsWarnings ? "WARNING" : "FAILURE");
      }}));
      success = treatErrorsAsWarnings;
    }

    return success;
  }

  private File getDefaultXsltFile() {
     return new File(getStyleCopPluginDirectory(), "StyleCopViolations.xslt");
  }

  private void generateHtmlReport() throws TransformerException, IOException {
    final String styleCopReportXslt = getRunnerParameters().get(StyleCopConstants.SETTINGS_XSLTFILE);
    final File xsltFile;
    if (StringUtil.isEmptyOrSpaces(styleCopReportXslt)) {
      xsltFile = getDefaultXsltFile();
    }
    else {
      xsltFile = new File(getCheckoutDirectory(), styleCopReportXslt);
    }

    if (!xsltFile.exists()) {
      getLogger().warning(xsltFile.getAbsolutePath() + " not found => won't generate html report");
      return;
    }

    getLogger().progressMessage("Generating HTML report");

    Source xmlSource = new StreamSource(myXmlReportFile);
    Source xsltSource = new StreamSource(xsltFile);
    final FileOutputStream reportFileStream = new FileOutputStream(myHtmlReportFile);

    try {
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer trans = transformerFactory.newTransformer(xsltSource);
      trans.setParameter("sourceRoot", getCheckoutDirectory() + "\\");
      trans.transform(xmlSource, new StreamResult(reportFileStream));
    } finally {
      reportFileStream.close();
    }

    myArtifactsWatcher.addNewArtifactsPath(myOutputDirectory.getPath() + "/*.html");
  }

    private File getStyleCopPluginDirectory() {
        return new File(this.getAgentConfiguration().getAgentPluginsDirectory(), "stylecop");
    }

    @NotNull
  @Override
  public BuildFinishedStatus getRunResult(final int exitCode) {
    String failMessage = null;
    Boolean success = true;

    if (myXmlReportFile.exists()) {

      myArtifactsWatcher.addNewArtifactsPath(myXmlReportFile.getPath());

      String styleCopPluginDir = getStyleCopPluginDirectory().getAbsolutePath();
      myArtifactsWatcher.addNewArtifactsPath(styleCopPluginDir + "/*.gif");

      try {
        success = importInspectionResults();
        generateHtmlReport();
      } catch (Exception e) {
        getLogger().error("Exception while importing stylecop results: " + e);
        failMessage = "StyleCop results import error";
      }
    }

    if (failMessage != null) {
      getLogger().buildFailureDescription(failMessage);
    }

    return failMessage != null || !success
           ? BuildFinishedStatus.FINISHED_FAILED
           : BuildFinishedStatus.FINISHED_SUCCESS;
  }

  @NotNull
  public ProgramCommandLine makeProgramCommandLine() throws RunBuildException {
    final Map<String, String> runParameters = getRunnerParameters();

    String[] include = splitFileWildcards(runParameters.get(StyleCopConstants.SETTINGS_FILES));
    String[] exclude = splitFileWildcards(runParameters.get(StyleCopConstants.SETTINGS_FILESEXCLUDE));

    List<String> includeFiles, excludeFiles;

    try {
        includeFiles = matchFiles(include, exclude);
      } catch (IOException e) {
        throw new RunBuildException("I/O error while collecting include files", e);
      }

      if (includeFiles == null || includeFiles.size() == 0) {
        throw new RunBuildException("No files matched the include pattern");
      }

      try {
        excludeFiles = matchFiles(exclude, new String[0]);
      } catch (IOException e) {
        throw new RunBuildException("I/O error while collecting exclude files", e);
      }

    final StyleCopCommandLineBuilder commandLineBuilder = new StyleCopCommandLineBuilder(myXmlReportFile, includeFiles, excludeFiles, this.getAgentConfiguration(), runParameters, getCheckoutDirectory());

    return new ProgramCommandLine() {
      @NotNull
      public String getExecutablePath() throws RunBuildException {
        return commandLineBuilder.getExecutablePath();
      }

      @NotNull
      public String getWorkingDirectory() throws RunBuildException {
        return getCheckoutDirectory().getPath();
      }

      @NotNull
      public List<String> getArguments() throws RunBuildException {
        return commandLineBuilder.getArguments();
      }

      @NotNull
      public Map<String, String> getEnvironment() throws RunBuildException {
        return getBuildParameters().getEnvironmentVariables();
      }
    };
  }

  private List<String> matchFiles(String[] include, String[] exclude) throws IOException {
    final Map<String, String> runParameters = getRunnerParameters();

    final AntPatternFileFinder finder = new AntPatternFileFinder(
      include,
      exclude,
      SystemInfo.isFileSystemCaseSensitive);
    final File[] files = finder.findFiles(getCheckoutDirectory());

    getLogger().logMessage(DefaultMessagesInfo.createTextMessage("Matched files:"));

    final List<String> result = new ArrayList<String>(files.length);
    for (File file : files) {
      final String relativeName = FileUtil.getRelativePath(getWorkingDirectory(), file);

      result.add(relativeName);
      getLogger().logMessage(DefaultMessagesInfo.createTextMessage("  " + relativeName));
    }

    if (files.length == 0) {
      getLogger().logMessage(DefaultMessagesInfo.createTextMessage("  none"));
    }

    return result;
  }

  private static String[] splitFileWildcards(final String string) {
    if (string != null) {
      final String filesStringWithSpaces = string.replace('\n', ' ').replace('\r', ' ').replace('\\', '/');
      final List<String> split = StringUtil.splitCommandArgumentsAndUnquote(filesStringWithSpaces);
      return split.toArray(new String[split.size()]);
    }

    return new String[0];
  }
}

