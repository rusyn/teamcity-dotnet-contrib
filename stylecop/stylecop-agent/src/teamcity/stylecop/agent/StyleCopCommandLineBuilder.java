/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import jetbrains.buildServer.agent.BuildAgentConfiguration;
import jetbrains.buildServer.util.StringUtil;
import teamcity.stylecop.common.StyleCopConstants;

import java.io.File;
import java.util.*;

public class StyleCopCommandLineBuilder extends MSBuildCommandLineBuilder {

  private File myXmlReportFile;
  private List<String> myFilesToBeProcessed;
  private List<String> myFilesToBeExcluded;
  private Map<String, String> myRunParameters;
  private File myCheckoutDirectory;

  public StyleCopCommandLineBuilder(final File xmlReportFile, final List<String> includeFiles, List<String> excludeFiles, BuildAgentConfiguration buildAgentConfiguration, final Map<String, String> runParameters, File checkoutDirectory) {
      super(buildAgentConfiguration);
      myXmlReportFile = xmlReportFile;
      myFilesToBeProcessed = includeFiles;
      myFilesToBeExcluded =  excludeFiles;
      myRunParameters = runParameters;
      myCheckoutDirectory = checkoutDirectory;
  }

  @Override
  protected String getProjectFile()
  {
      File styleCopPluginDirectory = new File(myBuildAgentConfiguration.getAgentPluginsDirectory(), "stylecop");
      File projectFile = new File(styleCopPluginDirectory, "msbuild.xml");
      return projectFile.getPath();
  }

  private String GetProperty(String key, String value) {
      return String.format("/p:%s=%s", key, value);
  }

  @Override
  protected List<String> getProperties() {
   List<String> properties = new Vector<String>();

    // StyleCop path
    final String styleCopPathString = myRunParameters.get(StyleCopConstants.SETTINGS_STYLECOP_PATH);
    if (styleCopPathString != null) {
        properties.add(GetProperty("StyleCopPath", stripAllQuotes(styleCopPathString)));
    }

    // Files to analyse
    if (myFilesToBeProcessed != null && myFilesToBeProcessed.size() > 0) {
        StringBuilder fileArgument = new StringBuilder();
        for(int i=0; i<myFilesToBeProcessed.size(); i++){
            if (i != 0)
            {
                fileArgument.append("%3B");
            }

            File fileToBeProcessed = new File(myFilesToBeProcessed.get(i));
            if (!fileToBeProcessed.isAbsolute())
            {
                fileToBeProcessed = new File(myCheckoutDirectory, fileToBeProcessed.getPath());
            }

            fileArgument.append(fileToBeProcessed.getAbsolutePath());

        }
        properties.add(GetProperty("IncludeFiles", stripAllQuotes(fileArgument.toString())));
    }

    // Files to exclude
    if (myFilesToBeExcluded != null && myFilesToBeExcluded.size() > 0) {
        StringBuilder fileArgument = new StringBuilder();
        for(int i=0; i<myFilesToBeExcluded.size(); i++){
            if (i != 0)
            {
                fileArgument.append("%3B");
            }

            File fileToBeProcessed = new File(myFilesToBeExcluded.get(i));
            if (!fileToBeProcessed.isAbsolute())
            {
                fileToBeProcessed = new File(myCheckoutDirectory, fileToBeProcessed.getPath());
            }

            fileArgument.append(fileToBeProcessed.getAbsolutePath());

        }
        properties.add(GetProperty("ExcludeFiles", stripAllQuotes(fileArgument.toString())));
    }

    // Override settings file
    final String maxViolationCountString = myRunParameters.get(StyleCopConstants.SETTINGS_MAXVIOLATIONCOUNT);
    if (maxViolationCountString != null) {
      properties.add(GetProperty("MaxViolationCount", maxViolationCountString));
    }

    // Override settings file
    final String overrideSettingsFileString = myRunParameters.get(StyleCopConstants.SETTINGS_OVERRIDESETTINGSFILE);
    if (overrideSettingsFileString != null) {
      properties.add(GetProperty("OverrideSettingsFile", stripAllQuotes(overrideSettingsFileString)));
    }

    // Define constants
    final String defineConstantsString = myRunParameters.get(StyleCopConstants.SETTINGS_DEFINECONSTANTS);
    if (defineConstantsString != null) {
      properties.add(GetProperty("DefineConstants", escapeParameterValue(stripAllQuotes(defineConstantsString))));
    }

    // Additional addin paths
    final String additionalAddinPathsString = myRunParameters.get(StyleCopConstants.SETTINGS_ADDITIONALADDINPATHS);
    if (additionalAddinPathsString != null) {
        final List<String> relativeFolders = StringUtil.splitCommandArgumentsAndUnquote(additionalAddinPathsString);
        StringBuilder fileArgument = new StringBuilder();
        for(int i=0; i<relativeFolders.size(); i++){
            if (i != 0)
            {
                fileArgument.append("|");
            }
            fileArgument.append(new File(myCheckoutDirectory, relativeFolders.get(i)).getAbsolutePath());
        }
        properties.add(GetProperty("AdditionalAddinPaths", stripAllQuotes(fileArgument.toString())));
    }

    // Cache results
    final String cacheResultsString = myRunParameters.get(StyleCopConstants.SETTINGS_CACHERESULTS);
    if (cacheResultsString != null) {
      properties.add(GetProperty("CacheResults", cacheResultsString));
    }

    // Force full analysis
    final String forceFullAnalysisString = myRunParameters.get(StyleCopConstants.SETTINGS_FORCEFULLANALYSIS);
    if (forceFullAnalysisString != null) {
      properties.add(GetProperty("ForceFullAnalysis", forceFullAnalysisString));
    }

    // Treat errors as warnings
    final String treatErrorsAsWarningsString = myRunParameters.get(StyleCopConstants.SETTINGS_TREATERRORSASWARNINGS);
    if (treatErrorsAsWarningsString != null) {
      properties.add(GetProperty("TreatErrorsAsWarnings", treatErrorsAsWarningsString));
    }

    // Debug
    final String debugString = myRunParameters.get(StyleCopConstants.SETTINGS_DEBUG);
    if (debugString != null) {
      properties.add(GetProperty("Debug", debugString));
    }

    // Output file
    properties.add(GetProperty("OutputFile", stripAllQuotes(myXmlReportFile.getPath())));

    return properties;
  }

  private static String stripAllQuotes(String str)
  {
      return str.replace("\"", "");
  }
}