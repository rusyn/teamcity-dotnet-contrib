/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.BuildAgentConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public abstract class MSBuildCommandLineBuilder {

  protected final BuildAgentConfiguration myBuildAgentConfiguration;

  public MSBuildCommandLineBuilder(final BuildAgentConfiguration buildAgentConfiguration) {
    myBuildAgentConfiguration = buildAgentConfiguration;
  }

  @NotNull
  public String getExecutablePath() throws RunBuildException {
    File msBuildPath =  new File(getLatestDotNetFrameworkPath(), "msbuild.exe");
    return msBuildPath.getPath();
  }

  @NotNull
  public List<String> getArguments() throws RunBuildException {
    List<String> arguments = new Vector<String>();

    String projectFile = getProjectFile();
    if (projectFile != null && projectFile.length() > 0)
    {
        arguments.add(projectFile);
    }

    arguments.addAll(getProperties());

    arguments.add("/v:m");

    return arguments;
  }

  protected abstract List<String> getProperties();

  protected abstract String getProjectFile();

  @NotNull
  private File getLatestDotNetFrameworkPath() throws RunBuildException   {
      // Must be a nicer way to do this?
      Map<String, String> myConfigParameters = this.myBuildAgentConfiguration.getConfigurationParameters();

      String dotNet4x64Path = myConfigParameters.get("DotNetFramework4.0_x64_Path");
      if (dotNet4x64Path != null && dotNet4x64Path.length() > 0) {
          return new File(dotNet4x64Path);
      }

      String dotNet4x86Path = myConfigParameters.get("DotNetFramework4.0_x86_Path");
      if (dotNet4x86Path != null && dotNet4x86Path.length() > 0) {
          return new File(dotNet4x86Path);
      }

      String dotNet35x64Path = myConfigParameters.get("DotNetFramework3.5_x64_Path");
      if (dotNet35x64Path != null && dotNet35x64Path.length() > 0) {
          return new File(dotNet35x64Path);
      }

      String dotNet35x86Path = myConfigParameters.get("DotNetFramework3.5_x86_Path");
      if (dotNet35x86Path != null && dotNet35x86Path.length() > 0) {
          return new File(dotNet35x86Path);
      }

      String dotNet2x64Path = myConfigParameters.get("DotNetFramework2.0_x64_Path");
      if (dotNet2x64Path != null && dotNet2x64Path.length() > 0) {
          return new File(dotNet2x64Path);
      }

      String dotNet2x86Path = myConfigParameters.get("DotNetFramework2.0_x86_Path");
      if (dotNet2x86Path != null && dotNet2x86Path.length() > 0) {
          return new File(dotNet2x86Path);
      }

      throw new RuntimeException("Could not find a version of msbuild.exe on the machine");
  }

  @NotNull
  protected String escapeParameterValue(String parameterValue) {
      String escapedParameterValue = parameterValue.replace(';', '|');
      escapedParameterValue = escapedParameterValue.replace(' ', '|');
      escapedParameterValue = escapedParameterValue.replace(',', '|');
      return escapedParameterValue;
  }
}