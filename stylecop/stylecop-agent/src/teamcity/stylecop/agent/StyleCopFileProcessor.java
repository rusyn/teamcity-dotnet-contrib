/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.agent;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;

public class StyleCopFileProcessor {

  private final File myStylecopReport;

  private int myViolationsCount;

  public StyleCopFileProcessor(@NotNull final File stylecopReport) {
    myStylecopReport = stylecopReport;
   }

  public void processReport()throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
     DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
     domFactory.setNamespaceAware(true); // never forget this!
     DocumentBuilder builder = domFactory.newDocumentBuilder();
     Document document = builder.parse(myStylecopReport);

    XPathFactory factory = XPathFactory.newInstance();
    XPath xpath = factory.newXPath();
    XPathExpression expr = xpath.compile("count(//Violation)");

    double result = (Double) expr.evaluate(document, XPathConstants.NUMBER);
    myViolationsCount = (int) result;
  }

  public int getViolationsCount() {
    return myViolationsCount;
  }
}

