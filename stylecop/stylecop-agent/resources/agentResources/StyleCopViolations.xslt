﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>
  <xsl:param name="sourceRoot" />
  <xsl:key name="byNamespace" match="Violation" use="@RuleNamespace" />
  <xsl:key name="byNamespaceAndId" match="Violation" use="concat(@RuleNamespace, ':', @RuleId)" />

  <xsl:key name="bySource" match="Violation" use="@Source" />

  <xsl:template match="@* | node()">
    <html>
      <head>
        <style>
            body{border:0;width:100%;background:#fff;min-width:600px;font-family:tahoma,verdana,arial,sans-serif;font-size:82%;line-height:1.5em;margin:0;padding:0;}
            a{color:#0254D0;text-decoration:none;}
            a:hover{color:#fff;background:#369;text-decoration:none;}
            h1,h2,h3{margin:.8em 0 .2em;padding:0;}
            p{margin:.4em 0 .8em;padding:0;}
            table{font-size:90%;}
            #header{clear:both;float:left;width:100%;margin-bottom:20px;}
            .colmask{position:relative;clear:both;float:left;width:100%;overflow:hidden;}
            .colright,.colmid,.colleft{float:left;width:100%;position:relative;}
            .col1,.col2,.col3{float:left;position:relative;overflow:hidden;padding:0 0 1em;}
            .leftmenu{background:#fff;}
            .leftmenu .colleft{right:75%;}
            .leftmenu .col1{width:72%;left:75%;}
            .leftmenu .col2{left:3%;width:0;}
            #footer{clear:both;float:left;width:100%;}
            .error{color:Red;}
            .panel a{cursor:pointer;}
            ul{margin-left:0;padding-left:0;list-style:none;}
            ul li span{font-weight:700;cursor:pointer;padding-right:5px;}
            ul li span.closedGroup{padding-left:10px;background-image:url(plus.gif);background-repeat:no-repeat;background-position:left center;}
            ul li span.openGroup{padding-left:10px;background-image:url(minus.gif);background-repeat:no-repeat;background-position:left center;}
            #sourceCount{padding-right:0;}
            .panelLeft,.panelRight{border-top:1px solid silver;}
            ul li ul,#lineList li table{margin-left:20px;}
        </style>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js">&#160;</script>
        <script type="text/javascript">

          Event.observe(window, 'load', function() {
            // Rule Type Click
            $$("ul span").each(function(element) {
              Event.observe(element, 'click', function(){
                if (element.hasClassName('openGroup')) {
                  element.removeClassName('openGroup');
                  element.addClassName('closedGroup');
                  element.next().hide();
                }
                else {
                  element.removeClassName('closedGroup');
                  element.addClassName('openGroup');
                  element.next().show();
                }

              });

            });
            // Rule Type Click end
            
            // Rule Click
            $$("ul li a").each(function(element) {
              Event.observe(element, 'click', function(){
                var ruleId = element.readAttribute('data-ruleId');
                 hideRuleViolations(ruleId);
              });
            });
            // Rule Click end
            
         });
         
         function hideRuleViolations(ruleId) {
            var ruleLines = $$("#sourceList li");
            ruleLines.each(function(element) {
              if (element.readAttribute('data-ruleId') == ruleId)
              {
                element.hide();
                updateParent(element);
              }
            });
         }
         
         function updateParent(element) {
          updateSourceViolationCount(element.up());          
         }
         
         function updateSourceViolationCount(element) {         
          var count = 0;
          element.childElements().each(function(li) {
            alert('style' + li.getStyle('display'));
            if (li.getStyle('display') == 'none') {
              count=count+1;            
            }
          });
          
          element.childElements().first().remove();
          element.insert(count);
         }

        </script>
      </head>
      <body>
        <div id="header">
          Total Violations: <span class="error">
            <xsl:value-of select="count(//Violation)" />
          </span>
        </div>
        <div class="colmask leftmenu">
          <div class="colleft">
            <div class="col1">
              Violations
              <div class="panelRight">
                <ul id="sourceList">
                  <xsl:for-each select="Violation[generate-id(.) = generate-id(key('bySource', @Source)[1])]">
                    <xsl:sort select="@Source"/>
                    <xsl:variable name="Source" select="@Source" />
                    <xsl:variable name="sameSource" select="key('bySource', $Source)" />
                    <li>
                      <xsl:variable name="TrimmedSource">
                        <xsl:call-template name="string-replace-all">
                          <xsl:with-param name="text" select="@Source" />
                          <xsl:with-param name="replace" select="$sourceRoot" />
                          <xsl:with-param name="by" select="''" />
                        </xsl:call-template>
                      </xsl:variable>
                      <span class="closedGroup">
                        &#160;&#160;<xsl:value-of select="$TrimmedSource" />&#160;(<span id="sourceCount"><xsl:value-of select="count($sameSource)" /></span>)
                      </span>
                      <!-- Style set inline so that Prototype can access-->
                      <ul id="lineList" style="display:none">
                        <xsl:for-each select="$sameSource">
                          <xsl:sort select="@LineNumber" data-type="number"/>
                          <li>
                            <xsl:attribute name="data-ruleId">
                              <xsl:value-of select="@RuleId" />
                            </xsl:attribute>
                            <span class="closedGroup" style="font-weight:normal;">
                              &#160;&#160;<xsl:text>Line</xsl:text>&#160;<xsl:value-of select="@LineNumber" /><xsl:text>&#160;&#160;&#160;</xsl:text><xsl:value-of select="." />
                            </span>
                            <!-- Style set inline so that Prototype can access-->
                            <table style="display:none;">
                              <tr>
                                <td>Rule:</td>
                                <td>
                                  <xsl:value-of select="@Rule" />
                                </td>
                              </tr>
                              <tr>
                                <td>Rule Id:</td>
                                <td>
                                  <xsl:value-of select="@RuleId" />
                                </td>
                              </tr>
                              <tr>
                                <td>Rule Namespace:</td>
                                <td>
                                  <xsl:value-of select="@RuleNamespace" />
                                </td>
                              </tr>
                              <tr>
                                <td>Section:</td>
                                <td>
                                  <xsl:value-of select="@Section" />
                                </td>
                              </tr>
                              <tr>
                                <td>&#160;</td><td>&#160;</td>
                              </tr>
                            </table>
                          </li>
                        </xsl:for-each>
                      </ul>
                    </li>
                  </xsl:for-each>
                </ul>
              </div>     
            </div>
            <div class="col2">
              Rules
              <div class="panelLeft">
                <ul id="ruleTypeList">
                  <xsl:for-each select="Violation[generate-id(.) = generate-id(key('byNamespace', @RuleNamespace)[1])]">
                    <xsl:sort select="@RuleNamespace"/>
                    <li>
                      <xsl:variable name="RuleNamespace" select="@RuleNamespace" />
                      <xsl:variable name="sameNamespace" select="key('byNamespace', $RuleNamespace)" />
                      <span class="closedGroup">
                        &#160;&#160;<xsl:value-of select="$RuleNamespace" /> (<xsl:value-of select="count($sameNamespace)" />)
                      </span>
                      <!-- Style set inline so that Prototype can access-->
                      <ul id="ruleList" style="display:none;">
                        <xsl:for-each select="$sameNamespace[generate-id(.) = generate-id(key('byNamespaceAndId', concat($RuleNamespace, ':', @RuleId))[1])]">
                          <li>
                            <xsl:variable name="RuleId" select="@RuleId" />
                            <xsl:variable name="sameNamespaceAndId" select="key('byNamespaceAndId', concat($RuleNamespace, ':', $RuleId))" />
                            <a>
                              <xsl:attribute name="data-ruleId">
                                <xsl:value-of select="$RuleId" />
                              </xsl:attribute>
                              <xsl:value-of select="$RuleId" /><xsl:text> </xsl:text><xsl:value-of select="@Rule" />  (<xsl:value-of select="count($sameNamespaceAndId)" />)
                            </a>
                          </li>
                        </xsl:for-each>
                      </ul>
                    </li>
                  </xsl:for-each>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div id="footer">

        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
