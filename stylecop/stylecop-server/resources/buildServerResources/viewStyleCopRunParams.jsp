 <%--
  ~ Copyright 2010-2011 Martin MacPherson
  ~ Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
  ~ http://svn.jetbrains.org/teamcity/plugins/fxcop/
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  --%>

<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>
<jsp:useBean id="constants" class="teamcity.stylecop.server.StyleCopConstantsBean"/>

<div class="parameter">
   Files to analyse:
      <strong>
        <props:displayValue name="${constants.filesKey}" emptyValue="not specified"/>
      </strong>
      Files to exclude:
      <strong>
        <props:displayValue name="${constants.filesExcludeKey}" emptyValue="not specified"/>
      </strong>
</div>

<div class="parameter">
  StyleCop assembly path: <strong><props:displayValue name="${constants.styleCopPathKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
  Max violations: <strong><props:displayValue name="${constants.maxViolationCountKey}" emptyValue="0"/></strong>
</div>

<div class="parameter">
  Override settings file: <strong><props:displayValue name="${constants.overrideSettingsFileKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
  Define constants: <strong><props:displayValue name="${constants.defineConstantsKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
  Additional Addin Paths: <strong><props:displayValue name="${constants.additionalAddinPathsKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
  Cache results: <strong><props:displayCheckboxValue name="${constants.cacheResultsKey}" /></strong>
</div>

<div class="parameter">
  Force full analysis: <strong><props:displayCheckboxValue name="${constants.forceFullAnalysisKey}" /></strong>
</div>

<div class="parameter">
  Treat errors as warnings: <strong><props:displayCheckboxValue name="${constants.treatErrorsAsWarningsKey}" /></strong>
</div>

<div class="parameter">
  Show debug output: <strong><props:displayCheckboxValue name="${constants.debugKey}"/></strong>
</div>

<div class="parameter">
  Report XSLT file: <strong><props:displayValue name="${constants.reportXsltKey}" emptyValue="not specified"/></strong>
</div>