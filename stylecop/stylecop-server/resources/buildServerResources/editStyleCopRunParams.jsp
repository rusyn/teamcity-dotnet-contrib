 <%--
  ~ Copyright 2010-2011 Martin MacPherson
  ~ Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
  ~ http://svn.jetbrains.org/teamcity/plugins/fxcop/
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  --%>

<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>
<jsp:useBean id="constants" class="teamcity.stylecop.server.StyleCopConstantsBean"/>


<l:settingsGroup title="What to analyse">

  <tr>
    <th><label for="${constants.filesKey}">Files: <l:star/></label></th>
    <td><props:multilineProperty name="${constants.filesKey}"
                               linkTitle="Files or wildcards"
                               cols="40" rows="5"
                               expanded="true"/>
        <props:multilineProperty name="${constants.filesExcludeKey}"
                               linkTitle="Exclude files by wildcard"
                               cols="40" rows="3"
                               expanded="true"/>
      <span class="smallNote">Solution, project or code file names relative to checkout root separated by spaces.<br/>
        Ant-like wildcards are allowed.<br/>
        Example: bin\*.csproj</span>
      <span class="error" id="error_${constants.filesKey}"></span>
      <span class="error" id="error_${constants.filesExcludeKey}"></span>
    </td>
  </tr>

</l:settingsGroup>

<l:settingsGroup title="StyleCop location">
  <tr>
    <th><label for="${constants.styleCopPathKey}">StyleCop assembly path: <l:star/></label></th>
    <td><props:textProperty name="${constants.styleCopPathKey}" className="longField"/>
      <span class="error" id="error_${constants.styleCopPathKey}"></span>
      <span class="smallNote">Full path to StyleCop assembly.<br/>
       This will either be Microsoft.StyleCop.dll or StyleCop.dll if you are using v4.5 or later.</span>
    </td>
  </tr>
</l:settingsGroup>
<l:settingsGroup title="StyleCop options">
  <tr>
    <th><label for="${constants.maxViolationCountKey}">Max violations:</label></th>
    <td><props:textProperty name="${constants.maxViolationCountKey}" style="width:6em;" maxlength="12"/>
      <span class="error" id="error_${constants.maxViolationCountKey}"></span>
      <span class="smallNote">The maximum number of violations that can occur before the analysis is failed.<br/>
         Specifying 0 will cause StyleCop to use the default violation count limit.
         Specifying any positive number will cause StyleCop to use that number as the violation count limit.
         Specifying any negative number will cause StyleCop to allow any number of violations without limit.
      </span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.overrideSettingsFileKey}">Override settings file:</label></th>
    <td>
      <props:textProperty name="${constants.overrideSettingsFileKey}" className="longField"/>
      <span class="error" id="error_${constants.overrideSettingsFileKey}"></span>
      <span
          class="smallNote">The path to any override settings file.  This should be relative to the checkout root.</span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.defineConstantsKey}">Define constants:</label></th>
    <td>
      <props:textProperty name="${constants.defineConstantsKey}" className="longField"/>
      <span class="error" id="error_${constants.defineConstantsKey}"></span>
      <span
          class="smallNote">Configure any conditional compilation constants. These should be semi-colon separated.<br/>
          eg.. DEBUG;SILVERLIGHT</span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.additionalAddinPathsKey}">Additional Addin Paths:</label></th>
    <td>
      <props:multilineProperty name="${constants.additionalAddinPathsKey}"
                               linkTitle="Folder paths or wildcards"
                               cols="40" rows="5"
                               expanded="true"/>
      <span class="error" id="error_${constants.additionalAddinPathsKey}"></span>
      <span class="smallNote">The paths to any folder which may contain addins, relative to checkout root and separated by spaces.<br/>
        Ant-like wildcards are allowed.</span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.cacheResultsKey}">Cache results:</label></th>
    <td>
      <props:checkboxProperty name="${constants.cacheResultsKey}"/>
      <span class="error" id="error_${constants.cacheResultsKey}"></span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.forceFullAnalysisKey}">Force full analysis:</label></th>
    <td>
      <props:checkboxProperty name="${constants.forceFullAnalysisKey}"/>
      <span class="error" id="error_${constants.forceFullAnalysisKey}"></span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.treatErrorsAsWarningsKey}">Treat errors as warnings:</label></th>
    <td>
      <props:checkboxProperty name="${constants.treatErrorsAsWarningsKey}"/>
      <span class="error" id="error_${constants.treatErrorsAsWarningsKey}"></span>
    </td>
  </tr>
  <tr>
    <th><label for="${constants.debugKey}">Show debug output</label></th>
    <td>
      <props:checkboxProperty name="${constants.debugKey}"/>
      <span class="error" id="error_${constants.debugKey}"></span>
    </td>
  </tr>

</l:settingsGroup>

<l:settingsGroup title="Reporting">

  <tr>
    <th><label for="${constants.reportXsltKey}">Report XSLT file:</label></th>
    <td><props:textProperty name="${constants.reportXsltKey}" className="longField"/>
      <span class="error" id="error_${constants.reportXsltKey}"></span>
      <span class="smallNote">XSLT file used to generate HTML report.<br/>
        Leave it empty to get standard StyleCop report,<br/>
        or specify any custom file relative to checkout directory</span>
    </td>
  </tr>

</l:settingsGroup>