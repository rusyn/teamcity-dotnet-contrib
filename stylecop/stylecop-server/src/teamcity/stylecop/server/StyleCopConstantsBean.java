/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.server;

import teamcity.stylecop.common.StyleCopConstants;
import org.jetbrains.annotations.NotNull;

public class StyleCopConstantsBean {
  @NotNull
  public String getDebugKey() {
    return StyleCopConstants.SETTINGS_DEBUG;
  }

  @NotNull
  public String getCacheResultsKey() {
    return StyleCopConstants.SETTINGS_CACHERESULTS;
  }

  @NotNull
  public String getFilesKey() {
    return StyleCopConstants.SETTINGS_FILES;
  }

  @NotNull
  public String getFilesExcludeKey() {
    return StyleCopConstants.SETTINGS_FILESEXCLUDE;
  }

  @NotNull
  public String getDefineConstantsKey() {
    return StyleCopConstants.SETTINGS_DEFINECONSTANTS;
  }

  @NotNull
  public String getForceFullAnalysisKey() {
    return StyleCopConstants.SETTINGS_FORCEFULLANALYSIS;
  }

  @NotNull
  public String getMaxViolationCountKey() {
    return StyleCopConstants.SETTINGS_MAXVIOLATIONCOUNT;
  }

  @NotNull
  public String getOverrideSettingsFileKey() {
    return StyleCopConstants.SETTINGS_OVERRIDESETTINGSFILE;
  }

  @NotNull
  public String getTreatErrorsAsWarningsKey() {
    return StyleCopConstants.SETTINGS_TREATERRORSASWARNINGS;
  }

  @NotNull
  public String getReportXsltKey() {
    return StyleCopConstants.SETTINGS_XSLTFILE;
  }

  @NotNull
  public String getStyleCopPathKey() {
    return StyleCopConstants.SETTINGS_STYLECOP_PATH;
  }

  @NotNull
  public String getAdditionalAddinPathsKey() {
    return StyleCopConstants.SETTINGS_ADDITIONALADDINPATHS;
  }
}
