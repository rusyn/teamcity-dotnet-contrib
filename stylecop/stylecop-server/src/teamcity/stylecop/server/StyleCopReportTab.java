/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.server;

import jetbrains.buildServer.serverSide.SBuild;
import jetbrains.buildServer.serverSide.SBuildServer;
import jetbrains.buildServer.web.openapi.PagePlaces;
import jetbrains.buildServer.web.openapi.ViewLogTab;
import jetbrains.buildServer.web.reportTabs.ReportTabUtil;
import org.jetbrains.annotations.NotNull;
import teamcity.stylecop.common.StyleCopConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class StyleCopReportTab extends ViewLogTab {
  private static String TAB_TITLE = "StyleCop";
  private static String TAB_CODE = "styleCopTab";

  private static String TAB_STARTPAGE = StyleCopConstants.REPORT_FILE;

  public StyleCopReportTab(final PagePlaces pagePlaces, final SBuildServer server) {
    super(TAB_TITLE, TAB_CODE, pagePlaces, server);
    setIncludeUrl("/artifactsViewer.jsp");
  }

  protected void fillModel(final Map model, final HttpServletRequest request, final SBuild build) {
    model.put("basePath", "");
    model.put("startPage", TAB_STARTPAGE);
  }

  @Override
  protected boolean isAvailable(@NotNull HttpServletRequest request, @NotNull SBuild build) {
      return super.isAvailable(request, build) && ReportTabUtil.isAvailable(build, TAB_STARTPAGE);
  }
}
