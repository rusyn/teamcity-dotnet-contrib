/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.stylecop.server;

import jetbrains.buildServer.serverSide.InvalidProperty;
import jetbrains.buildServer.serverSide.PropertiesProcessor;
import jetbrains.buildServer.util.PropertiesUtil;
import teamcity.stylecop.common.StyleCopConstants;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class StyleCopRunTypePropertiesProcessor implements PropertiesProcessor {

  public Collection<InvalidProperty> process(Map<String, String> properties) {
    List<InvalidProperty> result = new Vector<InvalidProperty>();

    final String files = properties.get(StyleCopConstants.SETTINGS_FILES);

    if (PropertiesUtil.isEmptyOrNull(files)) {
      result.add(
        new InvalidProperty(
          StyleCopConstants.SETTINGS_FILES,
          "Files must be specified"));
    }

    final String styleCopPath = properties.get(StyleCopConstants.SETTINGS_STYLECOP_PATH);
    if (PropertiesUtil.isEmptyOrNull(styleCopPath)) {
      result.add(new InvalidProperty(StyleCopConstants.SETTINGS_STYLECOP_PATH, "StyleCop assembly path must be specified"));
    }
    else if (!styleCopPath.endsWith("dll")) {
        result.add(new InvalidProperty(StyleCopConstants.SETTINGS_STYLECOP_PATH, "StyleCop assembly path must be the full path to the StyleCop dll"));
    }

    return result;
  }
}
