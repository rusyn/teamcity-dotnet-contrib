/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.SystemInfo;
import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.BuildFinishedStatus;
import jetbrains.buildServer.agent.artifacts.ArtifactsWatcher;
import jetbrains.buildServer.agent.inspections.InspectionReporter;
import jetbrains.buildServer.agent.runner.BuildServiceAdapter;
import jetbrains.buildServer.agent.runner.ProgramCommandLine;
import jetbrains.buildServer.messages.DefaultMessagesInfo;
import jetbrains.buildServer.util.AntPatternFileFinder;
import jetbrains.buildServer.util.FileUtil;
import jetbrains.buildServer.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import teamcity.statlight.agent.params.*;
import teamcity.statlight.common.StatLightConstants;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StatLightBuildService extends BuildServiceAdapter {
    private final ArtifactsWatcher myArtifactsWatcher;
    private File myOutputDirectory;
    private File myXmlReportFile;
    private File myHtmlReportFile;

    public StatLightBuildService(final ArtifactsWatcher artifactsWatcher, final InspectionReporter inspectionReporter) {
        myArtifactsWatcher = artifactsWatcher;

    }

    @Override
    public void afterInitialized() throws RunBuildException {
        super.afterInitialized();

        try {
            myOutputDirectory = FileUtil.createTempFile(getBuildTempDirectory(), "statlight-output-", "", false);
            if (!myOutputDirectory.mkdirs()) {
                throw new RuntimeException("Unable to create temp output directory " + myOutputDirectory);
            }

            myXmlReportFile = new File(myOutputDirectory, StatLightConstants.OUTPUT_FILE);
            myHtmlReportFile = new File(myOutputDirectory, StatLightConstants.REPORT_FILE);
        } catch (IOException e) {
            final String message = "Unable to create temporary file in " +
                    getBuildTempDirectory() + " for statlight: " +
                    e.getMessage();

            Logger.getInstance(getClass().getName()).error(message, e);
            throw new RunBuildException(message);
        }
    }

    @Override
    public void beforeProcessStarted() throws RunBuildException {
        getLogger().progressMessage("Running StatLight");
    }

    private void generateHtmlReport() throws TransformerException, IOException {
        final String statLightReportXslt = getRunnerParameters().get(StatLightConstants.SETTINGS_XSLTFILE);
        if (StringUtil.isEmptyOrSpaces(statLightReportXslt)) {
            getLogger().message("Skipped html report generation since not requested");
            return;
        }

        final File xsltFile = new File(getCheckoutDirectory(), statLightReportXslt);
        if (!xsltFile.exists()) {
            getLogger().warning(xsltFile.getAbsolutePath() + " not found => won't generate html report");
            return;
        }

        getLogger().progressMessage("Generating HTML report");

        Source xmlSource = new StreamSource(myXmlReportFile);
        Source xsltSource = new StreamSource(xsltFile);
        final FileOutputStream reportFileStream = new FileOutputStream(myHtmlReportFile);

        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer trans = transformerFactory.newTransformer(xsltSource);

            trans.transform(xmlSource, new StreamResult(reportFileStream));
        } finally {
            reportFileStream.close();
        }

        myArtifactsWatcher.addNewArtifactsPath(myOutputDirectory.getPath() + "/*.html");
    }

    @NotNull
    @Override
    public BuildFinishedStatus getRunResult(final int exitCode) {
        String failMessage = null;

        if (exitCode != 0) {
            failMessage = "StatLight return code: " + exitCode;
        }

        if (myXmlReportFile.exists()) {
            myArtifactsWatcher.addNewArtifactsPath(myXmlReportFile.getPath());

            try {
                generateHtmlReport();
            } catch (Exception e) {
                getLogger().error("Exception while importing statlight results: " + e);
                failMessage = "StatLight results import error";
            }
        }

        if (failMessage != null) {
            getLogger().buildFailureDescription(failMessage);
        }

        return failMessage != null
                ? BuildFinishedStatus.FINISHED_FAILED
                : BuildFinishedStatus.FINISHED_SUCCESS;
    }

    @NotNull
    public ProgramCommandLine makeProgramCommandLine() throws RunBuildException {
        final Map<String, String> runParameters = getRunnerParameters();

        List<String> xapFiles;

        try {
            xapFiles = matchFiles();
        } catch (IOException e) {
            throw new RunBuildException("I/O error while collecting files", e);
        }

        if (xapFiles == null || xapFiles.size() == 0) {
            throw new RunBuildException("No files matched the pattern");
        }

        final List<String> finalXapFiles = xapFiles;

        ICommandLineParam[] paramHandlers = new ICommandLineParam[]{new XapPathParam(), new BrowserCountParam(), new DebugParam(),
                new DllPathParam(), new MethodsToTestParam(), new QueryStringParam(), new ReportParam(), new BrowserWindowParam(),
                new TagFilterParam(), new TeamCityParam(), new TestProviderParam(), new VersionParam()};

        final StatLightCommandLineBuilder commandLineBuilder = new StatLightCommandLineBuilder(runParameters, myXmlReportFile, paramHandlers);
        return new ProgramCommandLine() {
            @NotNull
            public String getExecutablePath() throws RunBuildException {
                return commandLineBuilder.getExecutablePath();
            }

            @NotNull
            public String getWorkingDirectory() throws RunBuildException {
                return getCheckoutDirectory().getPath();
            }

            @NotNull
            public List<String> getArguments() throws RunBuildException {
                return commandLineBuilder.getArguments(finalXapFiles);
            }

            @NotNull
            public Map<String, String> getEnvironment() throws RunBuildException {
                return getBuildParameters().getEnvironmentVariables();
            }
        };
    }

    private List<String> matchFiles() throws IOException {
        final Map<String, String> runParameters = getRunnerParameters();

        final AntPatternFileFinder finder = new AntPatternFileFinder(
                splitFileWildcards(runParameters.get(StatLightConstants.SETTINGS_FILES)),
                splitFileWildcards(runParameters.get(StatLightConstants.SETTINGS_FILESEXCLUDE)),
                SystemInfo.isFileSystemCaseSensitive);
        final File[] files = finder.findFiles(getCheckoutDirectory());

        getLogger().logMessage(DefaultMessagesInfo.createTextMessage("Matched xap and dll files:"));

        final List<String> result = new ArrayList<String>(files.length);
        for (File file : files) {
            final String relativeName = FileUtil.getRelativePath(getWorkingDirectory(), file);

            result.add(relativeName);
            getLogger().logMessage(DefaultMessagesInfo.createTextMessage("  " + relativeName));
        }

        if (files.length == 0) {
            getLogger().logMessage(DefaultMessagesInfo.createTextMessage("  none"));
        }

        return result;
    }

    private static String[] splitFileWildcards(final String string) {
        if (string != null) {
            final String filesStringWithSpaces = string.replace('\n', ' ').replace('\r', ' ').replace('\\', '/');
            final List<String> split = StringUtil.splitCommandArgumentsAndUnquote(filesStringWithSpaces);
            return split.toArray(new String[split.size()]);
        }

        return new String[0];
    }
}



