/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import teamcity.statlight.agent.params.ICommandLineParam;
import teamcity.statlight.common.StatLightConstants;

import java.io.File;
import java.util.*;

public class StatLightCommandLineBuilder {
    private final Map<String, String> runParameters;
    private final Map<String, ICommandLineParam> paramHandlerMap = new HashMap<String, ICommandLineParam>();
    private final List<String> arguments = new Vector<String>();
    private ICommandLineParam[] paramHandlers;
    private final File xmlReportFile;

    public StatLightCommandLineBuilder(final Map<String, String> runParameters, final File xmlReportFile, ICommandLineParam[] paramHandlers) {
        this.runParameters = runParameters;
        this.xmlReportFile = xmlReportFile;
        this.paramHandlers = paramHandlers;

        this.indexParamHandlers(paramHandlers);
    }

    @NotNull
    public String getExecutablePath() throws RunBuildException {
        final String statLightRootRelative = runParameters.get(StatLightConstants.SETTINGS_STATLIGHT_ROOT);
        if (StringUtil.isEmpty(statLightRootRelative)) {
            throw new RunBuildException("StatLight root not specified in build settings");
        }

        return new File(statLightRootRelative, StatLightConstants.STATLIGHT_BINARY).getPath();
    }

    @NotNull
    public List<String> getArguments(List<String> files) throws RunBuildException {
        // Files to test
        if (files != null) {
            ICommandLineParam xapParamHandler = this.paramHandlerMap.get(StatLightConstants.COMMANDLINE_XAPPATHSHORT);
            xapParamHandler.setIsInUse(true);
            for (String xapFile : new jetbrains.buildServer.util.filters.FilteredIterable<String>(files, new XapPathFilter())) {
                xapParamHandler.addValue(xapFile);
            }

            ICommandLineParam dllParamHandler = this.paramHandlerMap.get(StatLightConstants.COMMANDLINE_DLLPATHSHORT);
            dllParamHandler.setIsInUse(true);
            for (String dllFile : new jetbrains.buildServer.util.filters.FilteredIterable<String>(files, new DllPathFilter())) {
                dllParamHandler.addValue(dllFile);
            }
        }

        Iterator it = this.runParameters.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            String key = pairs.getKey().toString();
            String value = pairs.getValue().toString();
            if (!key.equals(StatLightConstants.SETTINGS_FILES) && !key.equals(StatLightConstants.SETTINGS_FILESEXCLUDE) && !key.equals(StatLightConstants.SETTINGS_COMMANDLINEPARAMS)) {
                ICommandLineParam handler = this.paramHandlerMap.get(key);
                if (handler != null && value != null) {
                    handler.setIsInUse(true);
                    if (!value.isEmpty()) {
                        handler.addValue(value);
                    }
                }
                //it.remove(); // avoids a ConcurrentModificationException
            }
        }

        // Need to run this last
        if (this.runParameters.containsKey(StatLightConstants.SETTINGS_COMMANDLINEPARAMS)) {
            this.processExtraArguments(this.runParameters.get(StatLightConstants.SETTINGS_COMMANDLINEPARAMS));
        }

        this.formatArguments();

        this.arguments.add("-r=\"" + this.xmlReportFile.getPath() + "\"");
        this.arguments.add("--teamcity");

        return this.arguments;
    }

    private void indexParamHandlers(ICommandLineParam[] paramHandlers) {
        for (ICommandLineParam paramHandler : paramHandlers) {
            if (!paramHandler.getKey().isEmpty()) {
                this.paramHandlerMap.put(paramHandler.getKey().toLowerCase(), paramHandler);
            }

            for (String key : paramHandler.getCommandLineKeys()) {
                this.paramHandlerMap.put(key.toLowerCase(), paramHandler);
            }
        }
    }

    private void processExtraArguments(String extraParams) {
        if (!extraParams.isEmpty()) {
            List<String> params = StringUtil.splitCommandArgumentsAndUnquote(extraParams);
            for (String param : params) {
                String key = param;
                String value = param;
                int separatorIndex = getSeparatorIndex(param);
                boolean isKeyValuePair = separatorIndex > 0;
                if (isKeyValuePair) {
                    key = param.substring(0, separatorIndex);
                    value = param.substring(separatorIndex + 1, param.length());
                }

                ICommandLineParam handler = this.paramHandlerMap.get(key.toLowerCase());
                if (handler != null) {
                    handler.setIsInUse(true);
                    handler.addValue(value);
                } else {
                    this.arguments.add(param);
                }
            }
        }
    }

    private int getSeparatorIndex(String parameter) {
        // Check first for equals
        int separatorIndex = parameter.indexOf('=');
        if (separatorIndex > 0) {
            return separatorIndex;
        }

        separatorIndex = parameter.indexOf(':');
        if (separatorIndex > 0) {
            return separatorIndex;
        }

        return -1;
    }

    private void formatArguments() {
        for (ICommandLineParam paramHandler : this.paramHandlers) {
            if (paramHandler.getIsInUse()) {
                this.arguments.addAll(paramHandler.getParams());
            }
        }
    }
}
