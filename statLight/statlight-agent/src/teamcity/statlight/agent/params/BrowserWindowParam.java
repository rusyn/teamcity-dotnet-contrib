package teamcity.statlight.agent.params;

import com.intellij.openapi.util.text.StringUtil;
import teamcity.statlight.common.StatLightConstants;

import java.util.List;
import java.util.Vector;

public class BrowserWindowParam extends CommandLineParamBase {
    Boolean hideBrowserWindow = true;
    String params;

    public void addValue(String s) {
        this.hideBrowserWindow = StringUtil.isEmpty(s) || s.equals(Boolean.FALSE.toString());
        Boolean isDefault = s.equals(Boolean.TRUE.toString()) ||
                s.toLowerCase().equals(StatLightConstants.COMMANDLINE_BROWSERWINDOW.toLowerCase()) ||
                s.toLowerCase().equals(StatLightConstants.COMMANDLINE_BROWSERWINDOWSHORT);
        if (! isDefault) {
            this.params = s;
        }
    }

    public String getKey() {
        return StatLightConstants.SETTINGS_BROWSERWINDOW;
    }

    public String[] getCommandLineKeys() {
        return new String[]{StatLightConstants.COMMANDLINE_BROWSERWINDOW, StatLightConstants.COMMANDLINE_BROWSERWINDOWSHORT};
    }

    public List<String> getParams() {
        List<String> results = new Vector<String>();
        if (!this.hideBrowserWindow) {
            if (StringUtil.isNotEmpty(this.params))
            {
                results.add(StatLightConstants.COMMANDLINE_BROWSERWINDOWSHORT + ":" + this.params);
            }
            else {
                results.add(StatLightConstants.COMMANDLINE_BROWSERWINDOWSHORT);
            }
        }

        return results;
    }
}
