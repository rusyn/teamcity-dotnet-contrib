/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent.params;

import teamcity.statlight.common.StatLightConstants;

import java.util.List;
import java.util.Vector;

public class TestProviderParam extends CommandLineParamBase {
    String testProvider;

    public void addValue(String s) {
        this.testProvider = s;
    }

    public String getKey() {
        return StatLightConstants.SETTINGS_TESTPROVIDER;
    }

    public String[] getCommandLineKeys() {
        return new String[]{StatLightConstants.COMMANDLINE_TESTPROVIDERSHORT, StatLightConstants.COMMANDLINE_TESTPROVIDER};
    }

    public List<String> getParams() {
        List<String> results = new Vector<String>();
        if (!StatLightConstants.TESTPROVIDER_AUTO.equals(this.testProvider)) {
            results.add(this.getKeyValuePair(StatLightConstants.COMMANDLINE_TESTPROVIDERSHORT, this.testProvider));
        }
        return results;
    }
}
