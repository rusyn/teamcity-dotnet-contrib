/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.server;

import teamcity.statlight.common.StatLightConstants;
import org.jetbrains.annotations.NotNull;

public class StatLightConstantsBean {

    @NotNull
    public String getDebugKey() {
        return StatLightConstants.SETTINGS_DEBUG;
    }

    @NotNull
    public String getCommandLineParamsKey() {
        return StatLightConstants.SETTINGS_COMMANDLINEPARAMS;
    }

    @NotNull
    public String getBrowserCountKey() {
        return StatLightConstants.SETTINGS_BROWSERCOUNT;
    }

    @NotNull
    public String getFilesKey() {
        return StatLightConstants.SETTINGS_FILES;
    }

    @NotNull
    public String getFilesExcludeKey() {
        return StatLightConstants.SETTINGS_FILESEXCLUDE;
    }

    @NotNull
    public String getMethodsToTestKey() {
        return StatLightConstants.SETTINGS_METHODSTOTEST;
    }

    @NotNull
    public String getQueryStringKey() {
        return StatLightConstants.SETTINGS_QUERYSTRING;
    }

    @NotNull
    public String getStatLightRootKey() {
        return StatLightConstants.SETTINGS_STATLIGHT_ROOT;
    }

    @NotNull
    public String getTagFiltersKey() {
        return StatLightConstants.SETTINGS_TAGFILTERS;
    }

    @NotNull
    public String getTestProviderKey() {
        return StatLightConstants.SETTINGS_TESTPROVIDER;
    }

    @NotNull
    public String getReportXsltKey() {
        return StatLightConstants.SETTINGS_XSLTFILE;
    }

    @NotNull
    public String getTestProviderAutoValue() {
        return StatLightConstants.TESTPROVIDER_AUTO;
    }

    @NotNull
    public String getTestProviderMSTestValue() {
        return StatLightConstants.TESTPROVIDER_MSTEST;
    }

    @NotNull
    public String getTestProviderNUnitValue() {
        return StatLightConstants.TESTPROVIDER_NUNIT;
    }

    @NotNull
    public String getTestProviderXUnitValue() {
        return StatLightConstants.TESTPROVIDER_XUNIT;
    }

    @NotNull
    public String getTestProviderUnitDrivenValue() {
        return StatLightConstants.TESTPROVIDER_UNITDRIVEN;
    }

    @NotNull
    public String getTestProviderXUnitLightValue() {
        return StatLightConstants.TESTPROVIDER_XUNITLIGHT;
    }

    @NotNull
    public String getTestProviderMSTestCustomValue() {
        return StatLightConstants.TESTPROVIDER_MSTESTCUSTOM;
    }

    @NotNull
    public String getVersionKey() {
        return StatLightConstants.SETTINGS_VERSION;
    }

    @NotNull
    public String getVersionAutoValue() {
        return StatLightConstants.VERSION_AUTO;
    }

    @NotNull
    public String getVersionNovember2009Value() {
        return StatLightConstants.VERSION_NOVEMBER2009;
    }

    @NotNull
    public String getVersionMarch2010Value() {
        return StatLightConstants.VERSION_MARCH2010;
    }

    @NotNull
    public String getVersionApril2010Value() {
        return StatLightConstants.VERSION_APRIL2010;
    }

    @NotNull
    public String getVersionMay2010Value() {
        return StatLightConstants.VERSION_MAY2010;
    }

    @NotNull
    public String getVersionFeb2011Value() {
        return StatLightConstants.VERSION_FEB2011;
    }

    @NotNull
    public String getBrowserWindowKey() {
        return StatLightConstants.SETTINGS_BROWSERWINDOW;
    }
}
