/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.server;

import jetbrains.buildServer.serverSide.InvalidProperty;
import jetbrains.buildServer.serverSide.PropertiesProcessor;
import teamcity.statlight.common.StatLightConstants;
import jetbrains.buildServer.util.PropertiesUtil;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class StatLightRunTypePropertiesProcessor implements PropertiesProcessor {

  public Collection<InvalidProperty> process(Map<String, String> properties) {
    List<InvalidProperty> result = new Vector<InvalidProperty>();

    final String files = properties.get(StatLightConstants.SETTINGS_FILES);

    if (PropertiesUtil.isEmptyOrNull(files)) {
      result.add(
        new InvalidProperty(
          StatLightConstants.SETTINGS_FILES,
          "Files must be specified"));
    }

    final String statlightRoot = properties.get(StatLightConstants.SETTINGS_STATLIGHT_ROOT);
    if (PropertiesUtil.isEmptyOrNull(statlightRoot)) {
      result.add(
        new InvalidProperty(
          StatLightConstants.SETTINGS_STATLIGHT_ROOT,
          "StatLight installation root must be specified"));
    }

    if (!PropertiesUtil.isEmptyOrNull(properties.get(StatLightConstants.SETTINGS_BROWSERCOUNT))) {
      Integer value = PropertiesUtil.parseInt(properties.get(StatLightConstants.SETTINGS_BROWSERCOUNT));
      if (value == null || value < 1) {
        result.add(new InvalidProperty(StatLightConstants.SETTINGS_BROWSERCOUNT, "Browser count must be a positive number"));
      }
    }

    return result;
  }
}
