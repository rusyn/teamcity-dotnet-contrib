/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.server;

import jetbrains.buildServer.serverSide.PropertiesProcessor;
import jetbrains.buildServer.serverSide.RunType;
import jetbrains.buildServer.serverSide.RunTypeRegistry;
import teamcity.statlight.common.StatLightConstants;
import jetbrains.buildServer.util.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class StatLightRunType extends RunType {
  public StatLightRunType(final RunTypeRegistry runTypeRegistry) {
    runTypeRegistry.registerRunType(this);
  }
    @Override
  public PropertiesProcessor getRunnerPropertiesProcessor() {
    return new StatLightRunTypePropertiesProcessor();
  }

  @Override
  public String getDescription() {
    return StatLightConstants.RUNNER_DESCRIPTION;
  }

  @Override
  public String getEditRunnerParamsJspFilePath() {
    return "editStatLightRunParams.jsp";
  }

  @Override
  public String getViewRunnerParamsJspFilePath() {
    return "viewStatLightRunParams.jsp";
  }

  @Override
  public Map<String, String> getDefaultRunnerProperties() {
    Map<String, String> map = new HashMap<String, String>();
    setupDefaultParams(map);
    return map;
  }

  @Override
  @NotNull
  public String getType() {
    return StatLightConstants.RUNNER_TYPE;
  }

  @Override
  public String getDisplayName() {
    return StatLightConstants.RUNNER_DISPLAY_NAME;
  }

  private static void setupDefaultParams(Map<String, String> parameters) {
    parameters.put(StatLightConstants.SETTINGS_BROWSERCOUNT, StatLightConstants.DEFAULT_BROWSERCOUNT);
    parameters.put(StatLightConstants.SETTINGS_TESTPROVIDER, StatLightConstants.DEFAULT_TESTPROVIDER);
    parameters.put(StatLightConstants.SETTINGS_VERSION, StatLightConstants.DEFAULT_VERSION);
  }

  @NotNull
  @Override
  public String describeParameters(@NotNull final Map<String, String> parameters) {
    StringBuilder result = new StringBuilder();
    result.append("Files: ").append(StringUtil.emptyIfNull(parameters.get(StatLightConstants.SETTINGS_FILES)));
    return result.toString();
  }
}
