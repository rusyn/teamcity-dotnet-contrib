<%--
~ Copyright 2010-2011 Martin MacPherson
~ Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
~ http://svn.jetbrains.org/teamcity/plugins/fxcop/
~
~ Licensed under the Apache License, Version 2.0 (the "License");
~ you may not use this file except in compliance with the License.
~ You may obtain a copy of the License at
~
~ http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing, software
~ distributed under the License is distributed on an "AS IS" BASIS,
~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
~ See the License for the specific language governing permissions and
~ limitations under the License.
--%>

<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>
<jsp:useBean id="constants" class="teamcity.statlight.server.StatLightConstantsBean"/>

<div class="parameter">
    XAP or dll files to test:
    <strong>
        <props:displayValue name="${constants.filesKey}" emptyValue="not specified"/>
    </strong>
    XAP or dll files to exclude:
    <strong>
        <props:displayValue name="${constants.filesExcludeKey}" emptyValue="not specified"/>
    </strong>
</div>

<div class="parameter">
    StatLight installation root: <strong><props:displayValue name="${constants.statLightRootKey}"
                                                             emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Test provider: <strong><props:displayValue name="${constants.testProviderKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Version: <strong><props:displayValue name="${constants.versionKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    No. of browser hosts: <strong><props:displayValue name="${constants.browserCountKey}" emptyValue="1"/></strong>
</div>

<div class="parameter">
    Additional test configuration: <strong><props:displayValue name="${constants.queryStringKey}"
                                                               emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Tag filters: <strong><props:displayValue name="${constants.tagFiltersKey}" emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Methods to test: <strong><props:displayValue name="${constants.methodsToTestKey}"
                                                 emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Show browser window: <strong><props:displayCheckboxValue name="${constants.browserWindowKey}"/></strong>
</div>

<div class="parameter">
    Show debug output: <strong><props:displayCheckboxValue name="${constants.debugKey}"/></strong>
</div>

<div class="parameter">
    Commandline parameters: <strong><props:displayValue name="${constants.commandLineParamsKey}"
                                                        emptyValue="not specified"/></strong>
</div>

<div class="parameter">
    Report XSLT file: <strong><props:displayValue name="${constants.reportXsltKey}"
                                                  emptyValue="not specified"/></strong>
</div>