<%--
~ Copyright 2010-2011 Martin MacPherson
~ Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
~ http://svn.jetbrains.org/teamcity/plugins/fxcop/
~
~ Licensed under the Apache License, Version 2.0 (the "License");
~ you may not use this file except in compliance with the License.
~ You may obtain a copy of the License at
~
~ http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing, software
~ distributed under the License is distributed on an "AS IS" BASIS,
~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
~ See the License for the specific language governing permissions and
~ limitations under the License.
--%>

<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>
<jsp:useBean id="constants" class="teamcity.statlight.server.StatLightConstantsBean"/>

<l:settingsGroup title="What to test">

    <tr>
        <th><label for="${constants.filesKey}">XAP or dll files: <l:star/></label></th>
        <td><props:multilineProperty name="${constants.filesKey}"
                                     linkTitle="XAP, dll files or wildcards"
                                     cols="60" rows="5"
                                     expanded="true"/>
            <props:multilineProperty name="${constants.filesExcludeKey}"
                                     linkTitle="Exclude XAP or dll files by wildcard"
                                     cols="60" rows="3"
                                     expanded="true"/>
      <span class="smallNote">XAP or dll file names relative to checkout root separated by spaces.<br/>
        Ant-like wildcards are allowed.<br/>
        Example: bin\*.xap Sample*.dll</span>
            <span class="error" id="error_${constants.filesKey}"></span>
            <span class="error" id="error_${constants.filesExcludeKey}"></span>
        </td>
    </tr>

</l:settingsGroup>

<l:settingsGroup title="StatLight location">

    <tr>
        <th><label for="${constants.statLightRootKey}">StatLight installation root: <l:star/></label></th>
        <td><props:textProperty name="${constants.statLightRootKey}" className="longField"/>
            <span class="error" id="error_${constants.statLightRootKey}"></span>
            <span class="smallNote">Place to call StatLight.exe from.</span>
        </td>
    </tr>

</l:settingsGroup>
<l:settingsGroup title="StatLight options">
    <script type="text/javascript">

    </script>
    <tr>
        <th><label for="${constants.testProviderKey}">Test provider:</label></th>
        <td>
            <c:set var="setVisibility">
                var testProviderSelect = document.forms[0]['prop:${constants.testProviderKey}'];
                var versionSelect = document.forms[0]['prop:${constants.versionKey}'];
                var tagFilterInput = document.forms[0]['prop:${constants.tagFiltersKey}'];
                if (testProviderSelect.getValue() == '${constants.testProviderMSTestValue}') {
                versionSelect.disabled = false;
                tagFilterInput.disabled = false;
                }
                else {
                if (testProviderSelect.getValue() == '${constants.testProviderMSTestCustomValue}') {
                tagFilterInput.disabled = false;
                }
                else {
                tagFilterInput.disabled = true;
                }

                versionSelect.disabled = true;
                }

                BS.VisibilityHandlers.updateVisibility('prop:${constants.versionKey}');
                BS.VisibilityHandlers.updateVisibility('prop:${constants.tagFiltersKey}');
            </c:set>
            <props:selectProperty id="${constants.testProviderKey}" name="${constants.testProviderKey}"
                                  onchange="${setVisibility}">
                <props:option value="${constants.testProviderAutoValue}">Auto</props:option>
                <props:option value="${constants.testProviderMSTestValue}">MSTest</props:option>
                <props:option
                        value="${constants.testProviderMSTestCustomValue}">MSTest with Custom Provider</props:option>
                <props:option value="${constants.testProviderNUnitValue}">NUnit</props:option>
                <props:option value="${constants.testProviderXUnitValue}">XUnit</props:option>
                <props:option value="${constants.testProviderXUnitLightValue}">XUnitLight</props:option>
                <props:option value="${constants.testProviderUnitDrivenValue}">Unit Driven</props:option>
            </props:selectProperty>
            <span class="error" id="error_${constants.testProviderKey}"></span>
            <span class="smallNote">Allows you to select the test provider.</span>
        </td>
    </tr>
    <tr id="VersionOptions">
        <th><label for="${constants.versionKey}">MSTest Version:</label></th>
        <td>
            <props:selectProperty id="${constants.versionKey}" name="${constants.versionKey}"
                disabled="${propertiesBean.properties[constants.testProviderKey] != constants.testProviderMSTestValue}">
                <props:option value="${constants.versionAutoValue}">Auto</props:option>
                <props:option value="${constants.versionNovember2009Value}">November 2009</props:option>
                <props:option value="${constants.versionMarch2010Value}">March 2010</props:option>
                <props:option value="${constants.versionApril2010Value}">April 2010</props:option>
                <props:option value="${constants.versionMay2010Value}">May 2010</props:option>
                <props:option value="${constants.versionFeb2011Value}">February 2011</props:option>
            </props:selectProperty><br/>
            <span class="error" id="error_${constants.versionKey}"></span>
            <span class="smallNote">Allows you to select the version of the MSTest test provider.</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.browserCountKey}">No. of browsers hosts:</label></th>
        <td><props:textProperty name="${constants.browserCountKey}" style="width:6em;" maxlength="12"/>
            <span class="error" id="error_${constants.browserCountKey}"></span>
            <span class="smallNote">The number of browser hosts to use when running the tests.</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.queryStringKey}">Additional test configuration:</label></th>
        <td>
            <props:textProperty name="${constants.queryStringKey}" className="longField"/>
            <span class="error" id="error_${constants.queryStringKey}"></span>
      <span
              class="smallNote">A method of passing extra configuration to your tests.<br/>
      Sets --QueryString: options for StatLight</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.tagFiltersKey}">Tag filters:</label></th>
        <td>
            <props:textProperty name="${constants.tagFiltersKey}" className="longField"
                                disabled="${(propertiesBean.properties[constants.testProviderKey] != constants.testProviderMSTestValue) && (propertiesBean.properties[constants.testProviderKey] != constants.testProviderMSTestCustomValue)}"/>
            <span class="error" id="error_${constants.tagFiltersKey}"></span>
      <span
              class="smallNote">The tag filter expression used to filter executed tests. This is only applicable for tests run using MSTest<br/>
      Sets /t: options for StatLight</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.methodsToTestKey}">Methods to test:</label></th>
        <td>
            <props:textProperty name="${constants.methodsToTestKey}" className="longField"/>
            <span class="error" id="error_${constants.methodsToTestKey}"></span>
      <span
              class="smallNote">Semi-colon separated list of method names (including namespaces) to execute.<br/>
      Sets --MethodsToTest: options for StatLight</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.browserWindowKey}">Show browser window:</label></th>
        <td>
            <props:checkboxProperty name="${constants.browserWindowKey}"/>
            <span class="error" id="error_${constants.browserWindowKey}"></span>
            <span class="smallNote">To customize the window size and state, use the commandline parameter field to override<br/>
                  Sets -b: options for StatLight</span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.debugKey}">Show debug output:</label></th>
        <td>
            <props:checkboxProperty name="${constants.debugKey}"/>
            <span class="error" id="error_${constants.debugKey}"></span>
        </td>
    </tr>
    <tr>
        <th><label for="${constants.commandLineParamsKey}">Commandline parameters:</label></th>
        <td><props:multilineProperty name="${constants.commandLineParamsKey}"
                                     linkTitle="Extra command line parameters or overrides"
                                     cols="60" rows="6"
                                     expanded="true"/>
          <span class="smallNote">Extra statlight commandline parameters. Values set here can be used to override values for fields above.<br/>
            Parameters should be space separated<br/>
            Example: --debug --QueryString=test -b:N800x600</span>
        </td>
    </tr>
</l:settingsGroup>

<l:settingsGroup title="Reporting">

    <tr>
        <th><label for="${constants.reportXsltKey}">Report XSLT file:</label></th>
        <td><props:textProperty name="${constants.reportXsltKey}" className="longField"/>
            <span class="error" id="error_${constants.reportXsltKey}"></span>
      <span class="smallNote">XSLT file used to generate HTML report.<br/>
        Leave it empty to get default test reporting,<br/>
        or specify any custom file relative to checkout directory for extra reporting.</span>
        </td>
    </tr>

</l:settingsGroup>