/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.common;

public interface StatLightConstants {

    String RUNNER_TYPE = "StatLight";
    String RUNNER_DISPLAY_NAME = "StatLight";
    String RUNNER_DESCRIPTION = "StatLight Silverlight Test Runner (.NET)";

    String STATLIGHT_BINARY = "StatLight.exe";
    String REPORT_FILE = "statlight-report.html";
    String OUTPUT_FILE = "statlight-result.xml";

    String SETTINGS_FILES = "statlight.files";
    String SETTINGS_FILESEXCLUDE = "statlight.filesexclude";
    String SETTINGS_STATLIGHT_ROOT = "statlight.root";
    String SETTINGS_TAGFILTERS = "statlight.tagfilters";
    String SETTINGS_METHODSTOTEST = "statlight.methodstotest";
    String SETTINGS_TESTPROVIDER = "statlight.testprovider";
    String SETTINGS_BROWSERCOUNT = "statlight.browsercount";
    String SETTINGS_QUERYSTRING = "statlight.querystring";
    String SETTINGS_DEBUG = "statlight.debug";
    String SETTINGS_XSLTFILE = "statlight.xsltfile";
    String SETTINGS_VERSION = "statlight.version";
    String SETTINGS_BROWSERWINDOW = "statlight.browserwindow";
    String SETTINGS_COMMANDLINEPARAMS = "statlight.commandlineparams";

    String TESTPROVIDER_AUTO = "auto";
    String TESTPROVIDER_MSTEST = "mstest";
    String TESTPROVIDER_NUNIT = "nunit";
    String TESTPROVIDER_XUNIT = "xunit";
    String TESTPROVIDER_UNITDRIVEN = "unitdriven";
    String TESTPROVIDER_XUNITLIGHT = "xunitlight";
    String TESTPROVIDER_MSTESTCUSTOM = "mstestwithcustomprovider";

    String DEFAULT_TESTPROVIDER = "auto";
    String DEFAULT_BROWSERCOUNT = "1";
    String DEFAULT_VERSION = "auto";

    String VERSION_AUTO = "auto";
    String VERSION_NOVEMBER2009 = "November2009";
    String VERSION_MARCH2010 = "March2010";
    String VERSION_APRIL2010 = "April2010";
    String VERSION_MAY2010 = "May2010";
    String VERSION_FEB2011 = "Feb2011";

    String COMMANDLINE_TAGFILTERS_SHORT = "-t";
    String COMMANDLINE_TAGFILTERS = "--TagFilters";
    String COMMANDLINE_METHODSTOTEST = "--MethodsToTest";
    String COMMANDLINE_BROWSERCOUNT = "--NumberOfBrowserHosts";
    String COMMANDLINE_QUERYSTRING = "--QueryString";
    String COMMANDLINE_BROWSERWINDOW = "--BrowserWindow";
    String COMMANDLINE_BROWSERWINDOWSHORT = "-b";
    String COMMANDLINE_DEBUG = "--debug";
    String COMMANDLINE_REPORTSHORT = "-r";
    String COMMANDLINE_REPORT = "--ReportOutputFile";
    String COMMANDLINE_TEAMCITY = "--teamcity";
    String COMMANDLINE_XAPPATHSHORT = "-x";
    String COMMANDLINE_XAPPATH = "--XapPath";
    String COMMANDLINE_DLLPATHSHORT = "-d";
    String COMMANDLINE_DLLPATH = "--DllPath";
    String COMMANDLINE_TESTPROVIDER = "--OverrideTestProvider";
    String COMMANDLINE_TESTPROVIDERSHORT = "-o";
    String COMMANDLINE_VERSION = "--Version";
    String COMMANDLINE_VERSIONSHORT = "-v";
}