/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import org.testng.annotations.Test;
import teamcity.statlight.common.StatLightConstants;

@Test
public class QueryStringTests extends StatLightCommandLineBuilderTestBase {
    public void testQueryString() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_QUERYSTRING, "filterA=1&filterB=2");
        assertCmdArgs("[--QueryString=\"filterA=1&filterB=2\"] ");
    }

    public void testQueryStringCommandLineParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_QUERYSTRING, "filterC=3&filterD=4");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--QueryString=filterA=1&filterB=2");
        assertCmdArgs("[--QueryString=\"filterA=1&filterB=2\"] ");
    }
}
