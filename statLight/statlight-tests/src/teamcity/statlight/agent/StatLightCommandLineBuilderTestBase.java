/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package teamcity.statlight.agent;

import jetbrains.buildServer.util.StringUtil;
import org.testng.annotations.BeforeMethod;
import teamcity.statlight.agent.params.*;
import teamcity.statlight.common.StatLightConstants;
import jetbrains.buildServer.BaseTestCase;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatLightCommandLineBuilderTestBase extends BaseTestCase {
    protected Map<String, String> myRunParameters;
    private File myXmlReportFile;
    private String myExpectedPostfix;

    @BeforeMethod
    @Override
    protected void setUp() throws Exception {
      super.setUp();

      myRunParameters = new HashMap<String, String>();
      myXmlReportFile = new File("C:\\a\\b");
      myExpectedPostfix = "[-r=\"" + myXmlReportFile.getPath() + "\"] [--teamcity] ";
    }

    protected void assertCmdArgs(final String expected) throws Exception {
        final String filesSetting = myRunParameters.get(StatLightConstants.SETTINGS_FILES);
        final List<String> files = filesSetting != null
                                   ? StringUtil.splitCommandArgumentsAndUnquote(filesSetting)
                                   : new ArrayList<String>();

        ICommandLineParam[] paramHandlers = new ICommandLineParam[]{new XapPathParam(), new BrowserCountParam(), new DebugParam(),
                        new DllPathParam(), new MethodsToTestParam(), new QueryStringParam(), new BrowserWindowParam(),
                        new TagFilterParam(), new TestProviderParam(), new VersionParam()};
        final StatLightCommandLineBuilder commandLineBuilder = new StatLightCommandLineBuilder(myRunParameters, myXmlReportFile,paramHandlers);
        final List<String> args = commandLineBuilder.getArguments(files);

        StringBuilder result = new StringBuilder();
        for (String chunk : args) {
          result.append("[").append(chunk).append("] ");
        }
        assertEquals(expected + myExpectedPostfix, result.toString());
    }

    protected void assertExecutablePath(final String expected) throws Exception {
      final StatLightCommandLineBuilder commandLineBuilder = new StatLightCommandLineBuilder(myRunParameters, myXmlReportFile,new ICommandLineParam[0]);
      final String executable = commandLineBuilder.getExecutablePath();

      assertEquals(expected, executable);
    }
}
