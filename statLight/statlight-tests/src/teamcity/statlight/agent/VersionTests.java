/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import org.testng.annotations.Test;
import teamcity.statlight.common.StatLightConstants;

@Test
public class VersionTests extends StatLightCommandLineBuilderTestBase {
    public void testVersion() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "April2010");
        assertCmdArgs("[-v=April2010] ");
    }

    public void testVersionAuto() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "auto");
        assertCmdArgs("");
    }

    public void testVersionWithCommandParamShortcut() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "April2010");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "-v=May2010");
        assertCmdArgs("[-v=May2010] ");
    }

    public void testVersionWithCommandParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "April2010");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--Version=May2010");
        assertCmdArgs("[-v=May2010] ");
    }

    public void testVersionAutoWithCommandParamShortcut() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "April2010");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "-v=auto");
        assertCmdArgs("");
    }

    public void testVersionAutoWithCommandParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_VERSION, "April2010");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--Version=auto");
        assertCmdArgs("");
    }
}
