/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import org.testng.annotations.Test;
import teamcity.statlight.common.StatLightConstants;

@Test
public class DllPathTests extends StatLightCommandLineBuilderTestBase {
    public void testXapFileMultiple() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] ");
    }

    public void testDllPathMultipleWithQuotes() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "\"my dir \\file1.dll\" file2.dll");
        assertCmdArgs("[-d=\"my dir \\file1.dll\"] [-d=\"file2.dll\"] ");
    }

    public void testDllPathMultipleWithCommandParamShortcut() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "-d=file3.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] [-d=\"file3.dll\"] ");
    }

    public void testDllPathMultipleWithCommandParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--DllPath=file3.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] [-d=\"file3.dll\"] ");
    }

    public void testCommandParamsWithDuplicate() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--DllPath=file2.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] ");
    }

    public void testDllMultipleWithCommandParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--Dll=file3.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] [-d=\"file3.dll\"] ");
    }

    public void testDllCommandParamsWithDuplicate() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_FILES, "file1.dll file2.dll");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--Dll=file2.dll");
        assertCmdArgs("[-d=\"file1.dll\"] [-d=\"file2.dll\"] ");
    }

}
