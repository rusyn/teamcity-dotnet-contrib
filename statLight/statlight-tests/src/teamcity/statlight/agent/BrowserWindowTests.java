/*
 * Copyright 2010-2011 Martin MacPherson
 * Based on FxCop Build Runner (Copyright Jetbrains s.r.o)
 * http://svn.jetbrains.org/teamcity/plugins/fxcop/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package teamcity.statlight.agent;

import org.testng.annotations.Test;
import teamcity.statlight.common.StatLightConstants;

@Test
public class BrowserWindowTests extends StatLightCommandLineBuilderTestBase {
    public void testBrowserWindow() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_BROWSERWINDOW, "true");
        assertCmdArgs("[-b] ");
    }

    public void testBrowserWindowCommandLineParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_BROWSERWINDOW, "true");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--BrowserWindow");
        assertCmdArgs("[-b] ");
    }

    public void testBrowserWindowCustomCommandLineParam() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_BROWSERWINDOW, "true");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--BrowserWindow:N800x600");
        assertCmdArgs("[-b:N800x600] ");
    }

    public void testBrowserWindowCommandLineParamShort() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_BROWSERWINDOW, "true");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "-b");
        assertCmdArgs("[-b] ");
    }

    public void testBrowserWindowCustomCommandLineParamShort() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_BROWSERWINDOW, "true");
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "-b:N800x600");
        assertCmdArgs("[-b:N800x600] ");
    }

    public void testBrowserWindowCustomCommandLineParamNoOverride() throws Exception {
        myRunParameters.put(StatLightConstants.SETTINGS_COMMANDLINEPARAMS, "--browserWindow:N800x600");
        assertCmdArgs("[-b:N800x600] ");
    }
}
