Readme for TeamCity Build Runners
=================================
                 
Please unzip the download and copy the contents (either statlight.zip or stylecop.zip) into the plugins folder located
in your TeamCity data directory. By default this is either 'C:\Documents and Settings\<user_name>\.BuildServer' or
'C:\Users\<user_name>\.BuildServer' on a Windows machine. The data directory can be confirmed by visiting the Server Configuration
page in the TeamCity UI (Administration->Service Configuration->General tab)

Restart TeamCity for the plugin to become available.

Any feedback welcome!

Development
=================================
Set a Path Variable (File > Settings) to the root of a TeamCity installation.


Changes
-------

17/05/2012   StatLight v1.2.0
Fix issue with positive StatLight return code not causing build failure
Update to handle dlls as well as xap files
Changed ShowBrowserHost parameter to BrowserWindow
Added ability to specify extra parameters and override
Added some extra test providers and mstest versions
------------------------------------------------------------------------------------------------------
15/02/2012   Teamcity v7 compatible versions: Stylecop.1.1.1 and StatLight.1.1.1
28/03/2013   Compiled against TeamCity 7.1.4 (build 24331).
             Upgraded to JUnit 4 / jMock 2. Moved JUnit dependencies to project location.